version: '3'
services:
  #Administration Api
  promocode-factory-administration-api:
    build: src/Otus.Teaching.Pcf.Administration/
    container_name: 'promocode-factory-administration-api'
    restart: always
    ports:
      - "8091:80"
    environment:
      - "ConnectionStrings:PromocodeFactoryAdministrationDb=Host=promocode-factory-administration-db;Database=promocode_factory_administration_db;Username=postgres;Password=docker"
      - "RabbitMq:Host=rabbitmq"
      - "RabbitMq:VirtualHost=/"
      - "RabbitMq:Username=rmuser"
      - "RabbitMq:Password=rmpassword"
    depends_on: 
      promocode-factory-administration-db:
        condition: service_started
      rabbitmq:
        condition: service_healthy
  #Administration Db
  promocode-factory-administration-db:
    image: "postgres:9.6.17"
    container_name: 'promocode-factory-administration-db'
    restart: always 
    ports:
      - 5433:5432
    environment: 
      - POSTGRES_PASSWORD=docker

  #ReceivingFromPartner Api
  promocode-factory-receiving-from-partner-api:
    build: src/Otus.Teaching.Pcf.ReceivingFromPartner/
    container_name: 'promocode-factory-receiving-from-partner-api'
    restart: always
    ports:
      - "8092:80"
    environment:
      - "ConnectionStrings:PromocodeFactoryReceivingFromPartnerDb=Host=promocode-factory-receiving-from-partner-db;Database=promocode_factory_receiving_from_partner_db;Username=postgres;Password=docker"
      - "IntegrationSettings:GivingToCustomerApiUrl=http://promocode-factory-giving-to-customer-api"
      - "IntegrationSettings:AdministrationApiUrl=http://promocode-factory-administration-api"
      - "RabbitMq:Host=rabbitmq"
      - "RabbitMq:VirtualHost=/"
      - "RabbitMq:Username=rmuser"
      - "RabbitMq:Password=rmpassword"
    depends_on: 
      promocode-factory-receiving-from-partner-db:
        condition: service_started
      rabbitmq:
        condition: service_healthy
  #ReceivingFromPartner Db
  promocode-factory-receiving-from-partner-db:
    image: "postgres:9.6.17"
    container_name: 'promocode-factory-receiving-from-partner-db'
    restart: always 
    ports:
      - 5434:5432
    environment: 
      - POSTGRES_PASSWORD=docker
  
  #GivingToCustomer Api
  promocode-factory-giving-to-customer-api:
    build: src/Otus.Teaching.Pcf.GivingToCustomer/
    container_name: 'promocode-factory-giving-to-customer-api'
    restart: always
    ports:
      - "8093:80"
    environment:
      - "ConnectionStrings:PromocodeFactoryGivingToCustomerDb=Host=promocode-factory-giving-to-customer-db;Database=promocode_factory_giving_to_customer_db;Username=postgres;Password=docker"
      - "RabbitMq:Host=rabbitmq"
      - "RabbitMq:VirtualHost=/"
      - "RabbitMq:Username=rmuser"
      - "RabbitMq:Password=rmpassword"
    depends_on: 
      promocode-factory-giving-to-customer-db:
        condition: service_started
      rabbitmq:
        condition: service_healthy
  #GivingToCustomer Db
  promocode-factory-giving-to-customer-db:
    image: "postgres:9.6.17"
    container_name: 'promocode-factory-giving-to-customer-db'
    restart: always 
    ports:
      - 5435:5432
    environment: 
      - POSTGRES_PASSWORD=docker
      
  rabbitmq:
    image: rabbitmq:3.12.0-management
    hostname: rabbitmq
    restart: always
    volumes:
      - rabbitMqData:/var/lib/rabbitmq
    environment:
      - RABBITMQ_DEFAULT_VHOST=/
      - RABBITMQ_DEFAULT_USER=rmuser
      - RABBITMQ_DEFAULT_PASS=rmpassword
      - RABBITMQ_SERVER_ADDITIONAL_ERL_ARGS=-rabbit log_levels [{connection,error},{default,error}] disk_free_limit 2147483648
    ports:
      - 15672:15672
      - 5672:5672
    healthcheck:
      #test: rabbitmq-diagnostics -q ping
      test: [ "CMD", "rabbitmqctl", "status", "-f", "http://localhost:15672"]
      interval: 5s
      timeout: 10s
      retries: 10
  
volumes:
  rabbitMqData: {}