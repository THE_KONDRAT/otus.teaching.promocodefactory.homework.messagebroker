﻿using MassTransit;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using System;
using Integration.RabbitMqContracts;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class EventConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IEmployeeService _employeeService;

        public EventConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            await _employeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
        }
    }
}
