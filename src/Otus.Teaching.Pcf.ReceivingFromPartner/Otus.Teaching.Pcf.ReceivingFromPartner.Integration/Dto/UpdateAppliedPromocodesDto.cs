﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class UpdateAppliedPromocodesDto
    {
        public Guid EmployeeId { get; set; }
    }
}
