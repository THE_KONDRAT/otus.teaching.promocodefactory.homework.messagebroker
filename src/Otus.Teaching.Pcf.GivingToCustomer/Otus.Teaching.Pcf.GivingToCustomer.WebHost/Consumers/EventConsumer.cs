﻿using MassTransit;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using System;
using Integration.RabbitMqContracts;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class EventConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IPromoCodesService _promoCodesService;

        public EventConsumer(IPromoCodesService promoCodesService)
        {
            _promoCodesService = promoCodesService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            await _promoCodesService.GivePromoCodesToCustomersWithPreferenceAsync(PromoCodeMapper.MapFromDto(context.Message));
        }
    }
}
