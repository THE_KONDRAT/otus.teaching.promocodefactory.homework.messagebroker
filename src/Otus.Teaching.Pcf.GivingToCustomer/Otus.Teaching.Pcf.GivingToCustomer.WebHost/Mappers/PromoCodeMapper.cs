﻿using System;
using System.Globalization;
using Integration.RabbitMqContracts;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

 namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest request) {

            var promocode = new PromoCode();
            promocode.Id = request.PromoCodeId;
            
            promocode.PartnerId = request.PartnerId;
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;
           
            promocode.BeginDate = DateTime.Parse(request.BeginDate);
            promocode.EndDate = DateTime.Parse(request.EndDate);

            return promocode;
        }
        public static PromoCode MapFromDto(GivePromoCodeToCustomerDto request) {

            var promocode = new PromoCode();
            promocode.Id = request.PromoCodeId;
            
            promocode.PartnerId = request.PartnerId;
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;

            promocode.BeginDate = DateTime.ParseExact(request.BeginDate, "d", CultureInfo.InvariantCulture);
            promocode.EndDate = DateTime.ParseExact(request.EndDate, "d", CultureInfo.InvariantCulture);

            promocode.PreferenceId = request.PreferenceId;
            return promocode;
        }
    }
}
